import os
from time import sleep

from selenium.webdriver import Chrome
from selenium.webdriver.common.keys import Keys

SELEDINE_URL = 'selenide.org'
SELEDINE = 'selenide'
CURRENT_DIR = os.path.dirname(__file__)
# делал на убунту. если будет другой драйвер заменить на другой файл.
CHROME_DRIVER = 'chromedriver'
PATH_TO_DRIVER = os.path.join(CURRENT_DIR, CHROME_DRIVER)
GOOGLE_URL = 'http://google.com/ncr'


def find_seledine_link(driver):
    div = driver.find_element_by_css_selector('div.g')
    first_element_href = div.find_element_by_tag_name(
        'a').get_attribute('href')
    return (
        f'Первая сслыка это {SELEDINE_URL} ({first_element_href})'
        if SELEDINE in first_element_href
        else f'Первая ссылка не {SELEDINE_URL}'
    )


def main():
    driver = Chrome(executable_path=PATH_TO_DRIVER)
    driver.get(url=GOOGLE_URL)
    search_string = driver.find_element_by_name('q')
    search_string.send_keys('selenide')
    search_string.send_keys(Keys.RETURN)
    print(find_seledine_link(driver))
    driver.find_element_by_link_text('Images').click()
    div = driver.find_element_by_css_selector('div.islrc')
    first_image_alt = div.find_element_by_tag_name('img').get_attribute('alt')
    if SELEDINE in first_image_alt.lower():
        print(f'Первая картинка связана с {SELEDINE} ({first_image_alt})')
    driver.find_element_by_link_text('Все').click()
    print(find_seledine_link(driver))
    sleep(5)
    driver.close()
    driver.quit()


if __name__ == '__main__':
    main()
