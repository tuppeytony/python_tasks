def sum_odd_numbers():
    result = 0
    for i in range(1, 101):
        if not i % 2:
            result += i
    return result


print(sum_odd_numbers())
