def list_filter():
    numbers = [0, 99, 100, 53, 44, 23, 4, 8, 16, 15, 77, 51]
    return tuple(filter(lambda x: x > 50 and x % 2, numbers))


print(list_filter())
