def dict_test():
    dict = {'one': 1, 'two': 2, 'thee': 3, 'four': 4, 'five': 5}
    return {i: dict[i] for i in dict if dict[i] >= 3}


print(dict_test())
