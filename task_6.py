class Animal:

    def voice(self):
        pass


class Dog(Animal):

    def voice(self):
        return 'ГАВ!'


class Cat(Animal):

    def voice(self):
        return 'Мяу!'


class Mouse(Animal):

    def voice(self):
        return 'Пи!'


cat = Cat()
dog = Dog()
mouse = Mouse()
print(cat.voice(), dog.voice(), mouse.voice(), sep='\n')
