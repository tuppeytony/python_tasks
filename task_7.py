class Animal:

    nums_instanses = 0

    def __init__(self):
        Animal.nums_instanses = Animal.nums_instanses + 1

    def voice(self):
        pass

    @staticmethod
    def instanses_count():
        return Animal.nums_instanses


class Dog(Animal):

    def voice(self):
        return 'ГАВ!'


class Cat(Animal):

    def voice(self):
        return 'Мяу!'


class Mouse(Animal):

    def voice(self):
        return 'Пи'


cat = Cat()
dog = Dog()
mouse = Mouse()
print(cat.voice(), dog.voice(), mouse.voice(),
      Animal.instanses_count(), sep='\n')
