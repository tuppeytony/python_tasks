import os


def reverse_file_text():
    current_dir = os.path.dirname(__file__)
    first_file_name = 'first_file.txt'
    created_file_name = 'created_file.txt'
    path_to_file = os.path.join(current_dir, first_file_name)
    path_to_created_file = os.path.join(current_dir, created_file_name)
    with open(path_to_file, 'r') as readed_file:
        first_file = readed_file.read()
        text_list = list(first_file)
        text_list.reverse()
        reverse_text = ''.join(text_list)
        with open(path_to_created_file, 'w+') as new_file:
            new_file.write(reverse_text)
        print(f'Исходный текст {first_file}',
              f'Текст {reverse_text} из нового файла',
              sep='\n')


reverse_file_text()
