from threading import Thread
from time import sleep


def range_numbers(thread_name):
    for i in range(10, 0, -1):
        print(thread_name, i, end='\n')
        sleep(1)


first_thread = Thread(
    target=range_numbers,
    name='first thread',
    daemon=True,
    args=('first thread',)
)
second_thread = Thread(
    target=range_numbers,
    name='second thread',
    daemon=True,
    args=('second thread',)
)

first_thread.start()
print(f'{first_thread.name} started')
second_thread.start()
print(f'{second_thread.name} started')
first_thread.join()
second_thread.join()
