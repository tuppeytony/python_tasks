from random import randint


def random_from_list():
    numbers = [0, 99, 100, 53, 44, 23, 4, 8, 16, 15, 77, 51]
    return numbers[randint(0, len(numbers) - 1)]


print(random_from_list())
